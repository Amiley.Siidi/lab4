package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  
  public final int row;
  int col;
  List<List<CellColor>> cell;

  public ColorGrid (int row, int col) {
    this.row = row;
    this.col = col;
    this.cell = new ArrayList<List<CellColor>>(row);

    for (int i = 0; i < row; i++) {
      this.cell.add(new ArrayList<CellColor>(col));
      for (int j = 0; j < col; j++) {
        this.cell.get(i).add(new CellColor(new CellPosition(i, j), null));
      }
    }
  }
  @Override
  public int rows() {
    return row;
  }

  @Override
  public int cols() {
    return col;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> grid = new ArrayList<CellColor>();

    for (int i = 0; i < row; i++) {
      for (int j = 0; j < col; j++) {
        grid.add(cell.get(i).get(j));
      }
    }
    return grid;
  }

  @Override
  public Color get(CellPosition pos) {
    return cell.get(pos.row()).get(pos.col()).color();
  }

  @Override
  public void set(CellPosition pos, Color color) {
    cell.get(pos.row()).set(pos.col(), new CellColor(pos, color));
  }

}