package no.uib.inf101.gridview;

import java.awt.Color;
import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
    public static void main(String[] args) {

        IColorGrid grid = new ColorGrid(0, 4); // Corrected syntax

        grid.set(new CellPosition(0, 0), Color.RED);
        grid.set(new CellPosition(0, 3), Color.BLUE);
        grid.set(new CellPosition(2, 0), Color.YELLOW);
        grid.set(new CellPosition(2, 3), Color.GREEN); // Corrected coordinates

        System.out.println(grid.get(new CellPosition(0, 0)));
        System.out.println(grid.get(new CellPosition(0, 3)));
        System.out.println(grid.get(new CellPosition(2, 0)));
        System.out.println(grid.get(new CellPosition(2, 3)));

        GridView canvas = new GridView(grid); // Corrected class name
        JFrame frame = new JFrame();
        frame.setContentPane(canvas);
        frame.setTitle("LAB_4"); // Corrected syntax
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
