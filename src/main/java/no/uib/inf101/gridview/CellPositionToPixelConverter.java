package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

    public class CellPositionToPixelConverter {
  // TODO: Implement this class
      private Rectangle2D box;
      private GridDimension gd;
      private double margin;

      public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp) {
        // Calculate cell width and height
        double cellWidth = (box.getWidth() - (gd.cols() + 1) * margin) / gd.cols();
        double cellHeight = (box.getHeight() - (gd.rows() + 1) * margin) / gd.rows();

        // Calculate cell position
        double cellX = box.getX() + margin + cp.col() * (cellWidth + margin);
        double cellY = box.getY() + margin + cp.row() * (cellHeight + margin);

        // Create and return the rectangle
        return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    }
    }
