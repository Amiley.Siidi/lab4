
package no.uib.inf101.gridview;

import java.awt.*;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.*;

public class GridView extends JPanel {
  
  IColorGrid colorGrid;
  private static double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.GRAY;

  public GridView (IColorGrid colorGrid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;
  }

  private void drawGrid (Graphics2D g2d) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;

    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);

    g2d.setColor(MARGINCOLOR);
    g2d.fill(rectangle);

    CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(rectangle, colorGrid, OUTERMARGIN);

    drawCells(g2d, colorGrid, cellPosition);
  }
  private static void drawCells (Graphics2D g2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cppc) {
    for (CellColor cell : cellColorCollection.getCells()) {

      Rectangle2D rectangle = cppc.getBoundsForCell(cell.cellPosition());

      if (cell.color() != null) {
        g2d.setColor(cell.color());
      }
      else {
        g2d.setColor(Color.DARK_GRAY);
      }
      g2d.fill(rectangle);
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D) g;

    drawGrid(g2d);
  }
}